//
//  UserAlbumData.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import <Foundation/Foundation.h>

#import "SingleUser.h"
#import "UserAlbumIdCoupling.h"
#import "UserAddress.h"
#import "GeoLocation.h"
#import "UserCompanyInfo.h"
#import "AlbumPhotoInfo.h"


@interface UserAlbumData : NSObject
{
    
    NSMutableArray      *   usersArray;  // Holds an array of SingleUser objects.
    NSMutableArray      *   albumPhotoInfoArray; // Holds an array of album photo objects.
    NSMutableArray      *   staticUsersArray;
    NSMutableArray      *   searchUsersArray;
}


@property (nonatomic, strong) NSMutableArray      *   usersArray;
@property (nonatomic, strong) NSMutableArray      *   albumPhotoInfoArray;
@property (nonatomic, strong) NSMutableArray      *   staticUsersArray;
@property (nonatomic, strong) NSMutableArray      *   searchUsersArray;

+ (instancetype)sharedInstance ;

- (void) loadSingleUser:(NSDictionary *)item;
- (void) loadAlbumData:(NSDictionary *)item ;
- (void) loadPhotoAlbumData:(NSDictionary *)item;
- (void) loadUserWithAlbumData:(NSDictionary *) item;
- (NSMutableArray *) retrieveAlbumPhotosInfo:(NSInteger)stagingTargetAlbumId;

- (void)swapToSearchModel;
- (void)loadSearchUser: (SingleUser *) discoveredUser;
- (void)loadStaticArray;
- (void)swapToOriginalModel;

@end
