//
//  UserAlbumData.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "UserAlbumData.h"

@implementation UserAlbumData


@synthesize usersArray;
@synthesize albumPhotoInfoArray;
@synthesize staticUsersArray;
@synthesize searchUsersArray;



+ (instancetype)sharedInstance  {
    
    static dispatch_once_t once;
    static id sharedInstance;
    
    dispatch_once(&once, ^{
        sharedInstance = [[self new] init];
    });
    
    return sharedInstance;
}




// setup the data collection
- init
{
    if (self = [super init])
    {
        [self prepUserAlbumData];
    }
    return self;
}




- (void)prepUserAlbumData {
    
    
    
    staticUsersArray = [[NSMutableArray alloc] init];
    [usersArray removeAllObjects];
    
    usersArray = [[NSMutableArray alloc] init];
    [usersArray removeAllObjects];
    
    albumPhotoInfoArray = [[NSMutableArray alloc] init];
    [albumPhotoInfoArray removeAllObjects];
    

    searchUsersArray  = [[NSMutableArray alloc] init];
    [searchUsersArray removeAllObjects];

}


- (NSMutableArray *) retrieveAlbumPhotosInfo:(NSInteger)stagingTargetAlbumId {
    
    NSMutableArray * returnedAssets = [[NSMutableArray alloc] init];
    [returnedAssets removeAllObjects];

    [albumPhotoInfoArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        AlbumPhotoInfo * tempAlbumPhoto = (AlbumPhotoInfo*)obj;
        
        if(tempAlbumPhoto.albumId == stagingTargetAlbumId){
            [returnedAssets addObject:tempAlbumPhoto];
        }
        
    }];
    
    return returnedAssets;
}


- (void) loadPhotoAlbumData:(NSDictionary *)item {
    
    
    AlbumPhotoInfo * tempSinglePhotoInfoNode = [[AlbumPhotoInfo alloc] init];
    tempSinglePhotoInfoNode.albumId      = [[item objectForKey:@"albumId"] integerValue];
    tempSinglePhotoInfoNode.photoId      = [[item objectForKey:@"id"]integerValue];
    tempSinglePhotoInfoNode.photoTitle   = [item objectForKey:@"title"];
    tempSinglePhotoInfoNode.photoUrl     =  [item objectForKey:@"url"];
    tempSinglePhotoInfoNode.thumbnailURL = [item objectForKey:@"thumbnailUrl"];
    
    [albumPhotoInfoArray addObject:tempSinglePhotoInfoNode];
}




- (void) loadAlbumData:(NSDictionary *)item
{
    [[UserAlbumData sharedInstance] loadUserWithAlbumData:item];

}




- (void) loadSingleUser:(NSDictionary *)item
{

    
    SingleUser * tempSingleUser = [[SingleUser alloc] init];
    
    tempSingleUser.userId = [[item objectForKey:@"id"] integerValue];
    tempSingleUser.name =  [item objectForKey:@"name"];
    tempSingleUser.email = [item objectForKey:@"email"];
    
    NSDictionary * tempCompDict = [item objectForKey:@"company"];
    tempSingleUser.companyInfo.catchPhrase = [tempCompDict objectForKey:@"catchPhrase"];
    
    [usersArray addObject:tempSingleUser];

}


- (void) loadUserWithAlbumData:(NSDictionary *) item
{
    NSInteger tempUserId = [[item objectForKey:@"userId"] intValue];
    [usersArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        SingleUser * tempUser = (SingleUser*)obj;
        if(tempUser.userId == tempUserId){
            [tempUser.userList addObject:item];
            [usersArray replaceObjectAtIndex:idx withObject:tempUser];
        }
    }];
}


- (void)loadStaticArray {
    [self.staticUsersArray addObjectsFromArray:self.usersArray];
}



-(void)loadSearchUser: (SingleUser *) discoveredUser {
    [self.searchUsersArray addObject:discoveredUser];
}



- (void)swapToSearchModel {
    [self.usersArray  removeAllObjects];
    [self.usersArray addObjectsFromArray:self.searchUsersArray];
}



- (void)swapToOriginalModel {
    [self.usersArray removeAllObjects];
    [self.usersArray  addObjectsFromArray:self.staticUsersArray];
    [self.searchUsersArray removeAllObjects];
}


@end
