//
//  SingleUser.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "SingleUser.h"

@implementation SingleUser

@synthesize userId;
@synthesize name;
@synthesize userName;
@synthesize email;
@synthesize addressInfo;
@synthesize phone;
@synthesize urlWebsite;
@synthesize companyInfo;
@synthesize userList;


- (id) init {
    if ((self = [super init])) {
        name = [[NSString alloc]init];
        userName = [[NSString alloc]init];
        email = [[NSString alloc]init];
        addressInfo = [[UserAddress alloc] init];
        phone = [[NSString alloc] init];
        urlWebsite = [[NSURL alloc] init];
        companyInfo = [[UserCompanyInfo alloc]init];
    
        userList = [[NSMutableArray alloc]init];
        [userList removeAllObjects];
    }
    return self;
}




@end
