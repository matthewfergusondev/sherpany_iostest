//
//  UserCompanyInfo.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "UserCompanyInfo.h"

@implementation UserCompanyInfo

@synthesize  name;
@synthesize  catchPhrase;
@synthesize  bs;


-(id) init
{
    if ((self = [super init])) {
        
        name = [[NSString alloc]init];
        catchPhrase = [[NSString alloc]init];
        bs = [[NSString alloc] init];
    
    }
    
    
    return self;
}

@end
