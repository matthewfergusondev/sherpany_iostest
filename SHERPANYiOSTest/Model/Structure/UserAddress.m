//
//  UserAddress.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "UserAddress.h"

@implementation UserAddress

@synthesize geo;
@synthesize street;
@synthesize suite;
@synthesize city;
@synthesize zipcode;


-(id) init
{
    if ((self = [super init])) {
        geo = [[GeoLocation alloc] init];
        street = [[NSString alloc] init];
        suite = [[NSString alloc] init];
        city = [[NSString alloc] init];
        zipcode = [[NSString alloc] init];
    }
    return self;
}


@end
