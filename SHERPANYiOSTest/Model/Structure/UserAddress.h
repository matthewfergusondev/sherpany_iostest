//
//  UserAddress.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import <Foundation/Foundation.h>
#import "GeoLocation.h"

@interface UserAddress : NSObject 

@property (nonatomic, strong) NSString * street;
@property (nonatomic, strong) NSString * suite;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * zipcode;
@property (nonatomic, strong) GeoLocation * geo;

@end
