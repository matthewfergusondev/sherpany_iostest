//
//  AlbumPhotoInfo.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "AlbumPhotoInfo.h"

@implementation AlbumPhotoInfo

@synthesize albumId;
@synthesize photoId;
@synthesize photoTitle;
@synthesize photoUrl;
@synthesize thumbnailURL;



- (id)init {
    if ((self = [super init])) {
        albumId         = 0;
        photoId         = 0;
        photoTitle      = [[NSString alloc]init];
        photoUrl        = [[NSString alloc] init];
        thumbnailURL    = [[NSString alloc] init];
    }
    return self;
}



@end
