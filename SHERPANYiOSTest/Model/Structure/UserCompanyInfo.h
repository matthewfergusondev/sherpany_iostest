//
//  UserCompanyInfo.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//



#import <Foundation/Foundation.h>

@interface UserCompanyInfo : NSObject {
    NSString * name;
    NSString * catchPhrase;
    NSString * bs;
}


@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* catchPhrase;
@property (nonatomic, strong) NSString * bs;

@end
