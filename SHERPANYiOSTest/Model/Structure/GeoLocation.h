//
//  GeoLocation.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import <Foundation/Foundation.h>

@interface GeoLocation : NSObject

@property (nonatomic, strong) NSString * lat;
@property (nonatomic, strong) NSString * lng;

@end
