//
//  UserAlbumIdCoupling.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import <Foundation/Foundation.h>


@interface UserAlbumIdCoupling : NSObject


@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger albumId;
@property (nonatomic, strong) NSString * albumtitle;

@end
