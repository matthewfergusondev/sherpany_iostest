//
//  GeoLocation.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "GeoLocation.h"

@implementation GeoLocation

@synthesize  lat;
@synthesize  lng;

-(id) init
{
    if ((self = [super init])) {
        lat = [[NSString alloc] init];
        lng = [[NSString alloc] init];
    }
    
    return self;
}

@end
