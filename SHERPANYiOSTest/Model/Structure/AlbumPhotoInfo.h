//
//  AlbumPhotoInfo.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import <Foundation/Foundation.h>

@interface AlbumPhotoInfo : NSObject

@property (nonatomic, assign) NSInteger         albumId;
@property (nonatomic, assign) NSInteger         photoId;
@property (nonatomic, strong) NSString      *   photoTitle;
@property (nonatomic, strong) NSString      *   photoUrl;
@property (nonatomic, strong) NSString      *   thumbnailURL;


@end
