//
//  SingleUser.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//


#import <Foundation/Foundation.h>
#import "UserCompanyInfo.h"
#import "UserAddress.h"


@interface SingleUser : NSObject

@property (nonatomic,assign) NSInteger userId;
@property (nonatomic, strong ) NSString * name;
@property (nonatomic, strong ) NSString * userName;
@property (nonatomic, strong ) NSString * email;
@property (nonatomic, strong ) UserAddress * addressInfo;
@property (nonatomic, strong ) NSString * phone;
@property (nonatomic, strong ) NSURL * urlWebsite;
@property (nonatomic, strong ) UserCompanyInfo * companyInfo;
@property (nonatomic, strong ) NSMutableArray * userList;


@end
