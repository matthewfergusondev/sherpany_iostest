//
//  AlbumNameTableViewCell.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import <UIKit/UIKit.h>
#import "UserAlbumIdCoupling.h"

@interface AlbumNameTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel * title;


- (void)setCellData:(NSDictionary *)aUser;


@end
