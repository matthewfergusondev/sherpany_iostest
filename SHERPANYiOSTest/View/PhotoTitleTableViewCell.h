//
//  PhotoTitleTableViewCell.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//  Copyright © 2016 MatthewFerguson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumPhotoInfo.h"

@interface PhotoTitleTableViewCell : UITableViewCell


@property (nonatomic, weak) IBOutlet UILabel   *photoTitle;
@property (nonatomic, weak) IBOutlet UIWebView *photoWebView;


- (void)setCellPhotos:(AlbumPhotoInfo *)aUser;


@end
