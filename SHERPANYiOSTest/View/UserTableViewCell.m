//
//  UserTableViewCell.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "UserTableViewCell.h"
#import "UserCompanyInfo.h"

@implementation UserTableViewCell


@synthesize  name;
@synthesize  email;
@synthesize  catchPhrase;


- (void)awakeFromNib
{
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}


// The cell node setter
// We implement this because the table cell values need
// to be updated when this property changes, and this allows
// for the changes to be encapsulated
- (void)setCellUser:(SingleUser *)aUser
{
    self.name.text = aUser.name;
    self.email.text = aUser.email;

    UserCompanyInfo * tempUci = aUser.companyInfo;
    self.catchPhrase.text = tempUci.catchPhrase;
}


@end
