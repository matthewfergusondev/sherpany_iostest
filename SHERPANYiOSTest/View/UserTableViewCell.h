//
//  UserTableViewCell.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import <UIKit/UIKit.h>
#import "SingleUser.h"


@interface UserTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet    UILabel * name;
@property (nonatomic, weak) IBOutlet    UILabel * email;
@property (nonatomic, weak) IBOutlet    UILabel * catchPhrase;

- (void)setCellUser:(SingleUser *)aUser;


@end
