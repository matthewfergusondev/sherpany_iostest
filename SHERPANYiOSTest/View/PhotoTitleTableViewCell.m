//
//  PhotoTitleTableViewCell.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "PhotoTitleTableViewCell.h"

@implementation PhotoTitleTableViewCell

@synthesize photoWebView;
@synthesize photoTitle;

- (void)awakeFromNib {
    // Initialization code
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}




- (void)setCellPhotos:(AlbumPhotoInfo *) aAlbumPhoto {

    self.photoTitle.text = aAlbumPhoto.photoTitle;

    self.photoWebView.scalesPageToFit = YES;
    self.photoWebView.autoresizesSubviews = YES;
    self.photoWebView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.photoWebView.userInteractionEnabled = NO;
    self.photoWebView.backgroundColor = [UIColor clearColor];
    self.photoWebView.opaque = NO;
    [self.photoWebView setOpaque:NO];
    self.photoWebView.delegate = (id)self;
    
    NSURL * url = [NSURL URLWithString:aAlbumPhoto.thumbnailURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad  timeoutInterval:30.0f];
    [self.photoWebView loadRequest:requestObj];
    
}






@end
