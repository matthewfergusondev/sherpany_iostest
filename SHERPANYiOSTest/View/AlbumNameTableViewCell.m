//
//  AlbumNameTableViewCell.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "AlbumNameTableViewCell.h"

@implementation AlbumNameTableViewCell

@synthesize title;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


// The cell node setter
// We implement this because the table cell values need
// to be updated when this property changes, and this allows
// for the changes to be encapsulated
- (void)setCellData:(NSDictionary *)aUserAlbumId {
    self.title.text = [aUserAlbumId objectForKey:@"title"];
}




@end
