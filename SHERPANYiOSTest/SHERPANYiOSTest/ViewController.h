//
//  ViewController.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import <UIKit/UIKit.h>
#import "UserAlbumData.h"

@interface ViewController : UIViewController
{
    NSURLSession *session;
    
    __block NSURLSessionDataTask *task1;
    __block NSURLSessionDataTask *task2;
    __block NSURLSessionDataTask *task3;
}

@property (nonatomic, strong) __block NSURLSessionDataTask *task1;
@property (nonatomic, strong) __block NSURLSessionDataTask *task2;
@property (nonatomic, strong) __block NSURLSessionDataTask *task3;

@property (nonatomic, strong) NSURLSession * session;


-(void) sendREST_GetUsers:(NSString *) urlString;
-(void) sendREST_GetAlbumPhoto:(NSString *) urlString;
-(void) sendREST_GetAlbums:(NSString *) urlString;

-(void) cancelTasks;

@end

