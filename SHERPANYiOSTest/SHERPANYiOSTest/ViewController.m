//
//  ViewController.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize session;
@synthesize task1, task2, task3;



- (void)viewDidLoad {
    [super viewDidLoad];
}



- (void) viewDidAppear:(BOOL)animated {
    
    [self sendREST_GetUsers:@"http://jsonplaceholder.typicode.com/users"];//task1
    [self sendREST_GetAlbums:@"http://jsonplaceholder.typicode.com/albums"];//task2
    [self sendREST_GetAlbumPhoto:@"http://jsonplaceholder.typicode.com/photos"];//task3
    [task1 resume];//start the processing
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}




-(void) handleHttpErrors:(NSHTTPURLResponse *)httpResponse {
    
    
    
    /*
     400 Bad Request
     The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).[14]
     */
    if([httpResponse statusCode] == 400) {
    }
    
    /*
     401 Unauthorized (RFC 7235)
     Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication.
     */
    else if([httpResponse statusCode] == 401){
    }
    
    /*
     402 Payment Required
     Reserved for future use. The original intention was that this code might be used as part of some form of digital cash or micropayment scheme, but that has not happened, and this code is not usually used. YouTube uses this status if a particular IP address has made excessive requests, and requires the person to enter a CAPTCHA.[citation needed]
     */
    else if([httpResponse statusCode] == 402) {
    }
    
    /*
     403 Forbidden
     The request was a valid request, but the server is refusing to respond to it. Unlike a 401 Unauthorized response, authenticating will make no difference.
     */
    else if([httpResponse statusCode] == 403) {
    }
    
    /*
     404 Not Found
     The requested resource could not be found but may be available again in the future. Subsequent requests by the client are permissible.
     */
    else if([httpResponse statusCode] == 404) {
    }
    
    /*
     500 Internal Server Error
     A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
     */
    else if([httpResponse statusCode] == 500){
    }
    
   [self cancelTasks];
    
}



-(void) cancelTasks 
{
 
    if (task1.state == NSURLSessionTaskStateRunning || task1.state == NSURLSessionTaskStateSuspended)
    {
        [task1 cancel];
    }
    if (task2.state == NSURLSessionTaskStateRunning || task2.state == NSURLSessionTaskStateSuspended)
    {
        [task2 cancel];
    }
    if (task3.state == NSURLSessionTaskStateRunning || task3.state == NSURLSessionTaskStateSuspended)
    {
        [task3 cancel];
    }
    
    [self performSegueWithIdentifier:@"Network_Trouble_Segue" sender:self];
    
}




-(void) recvAlbumPhotoData: (NSData *)localData
{
        
    NSError *error = nil;
    NSArray *jsonArray;
    
    jsonArray = [NSJSONSerialization
                 JSONObjectWithData:localData
                 options: (NSJSONReadingAllowFragments | NSJSONReadingMutableContainers)
                 error: &error];
    
    if (!jsonArray) {
        [self cancelTasks];
    }
    else {
        for(NSDictionary *item in jsonArray)
        {
            [[UserAlbumData sharedInstance]loadPhotoAlbumData:item];
        }
        
        [self performSegueWithIdentifier:@"Main_UsersPhotos_Segue" sender:self];
    }
}




-(void) sendREST_GetAlbumPhoto:(NSString *) urlString
{
    if([urlString length] <= 0) {
        // problems
        [self cancelTasks];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *requestUrl = [NSMutableURLRequest requestWithURL:url
                                                              cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                          timeoutInterval:20.0];

    [requestUrl setValue:@"Mozilla/5.0" forHTTPHeaderField:@"User-Agent"];
    [requestUrl addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    requestUrl.HTTPMethod = @"GET";
    
    session = [NSURLSession sharedSession];
    task3 = [session dataTaskWithRequest:requestUrl
        completionHandler: ^(NSData *rtndata, NSURLResponse *response, NSError *error)
    {
        if (error != nil && error.code == NSURLErrorTimedOut){
            [self cancelTasks];
        }
        else if (error != nil) {
            [self cancelTasks];
        }
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                              
        /*
        200 OK
        Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request, the response will contain an entity describing or containing the result of the action.
        */
        if([httpResponse statusCode] == 200)
        {
            if( ([rtndata length] > 0) && (error == nil) )
            {
                // now pass the data to a main threaded procedure, required by UIKit Responder chain,
                // parse the json data, persist message data
                [self performSelectorOnMainThread:@selector(recvAlbumPhotoData:)
                        withObject:rtndata waitUntilDone:NO];
            }
            else if ([rtndata length] == 0 && error == nil) {
                //[self performSelectorOnMainThread:@selector(emptyReply:) withObject:nil waitUntilDone:NO];
                [self cancelTasks];
            }
        }
    }];

}





-(void) recvAlbumData: (NSData *)localData
{
    
    NSError *error = nil;
    NSArray *jsonArray;
    
    jsonArray = [NSJSONSerialization
                 JSONObjectWithData:localData
                 options: (NSJSONReadingAllowFragments | NSJSONReadingMutableContainers)
                 error: &error];
    
    if (!jsonArray) {
        [self cancelTasks];
    }
    else {
        
        for(NSDictionary *item in jsonArray) {
            //load the data source for the table view
            [[UserAlbumData sharedInstance] loadAlbumData:item];
        }
        
        [[UserAlbumData sharedInstance] loadStaticArray]; //load an unchangeable data set
        
        [task3 resume];
    }
}




-(void) sendREST_GetAlbums:(NSString *) urlString
{
    if([urlString length] <= 0) {
        // problems
        [self cancelTasks];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *requestUrl = [NSMutableURLRequest requestWithURL:url
                                                              cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                          timeoutInterval:4.0];
    
    [requestUrl setValue:@"Mozilla/5.0" forHTTPHeaderField:@"User-Agent"];
    [requestUrl addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    requestUrl.HTTPMethod = @"GET";
    
    session = [NSURLSession sharedSession];
    task2 = [session dataTaskWithRequest:requestUrl
                       completionHandler: ^(NSData *rtndata, NSURLResponse *response, NSError *error)
    {
        if (error != nil && error.code == NSURLErrorTimedOut){
            [self cancelTasks];
            //[self performSelectorOnMainThread:@selector(timedOut:) withObject:nil waitUntilDone:NO];
        }
        else if (error != nil){
            [self cancelTasks];
        }
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                 
        /*
        200 OK
        Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request, the response will contain an entity describing or containing the result of the action.
        */
        if([httpResponse statusCode] == 200){
            if( ([rtndata length] > 0) && (error == nil) ){
                // now pass the data to a main threaded procedure, required by UIKit Responder chain,
                // parse the json data, persist message data
                [self performSelectorOnMainThread:@selector(recvAlbumData:)
                            withObject:rtndata waitUntilDone:NO];
                         
            }
            else if ([rtndata length] == 0 && error == nil) {
                //[self performSelectorOnMainThread:@selector(emptyReply:) withObject:nil waitUntilDone:NO];
                [self cancelTasks];
            }
        }
    }];
    
}




-(void) recvUsersData: (NSData *)localData
{

    NSError *error = nil;
    NSArray *jsonArray;
    
    jsonArray = [NSJSONSerialization
                     JSONObjectWithData:localData
                     options: (NSJSONReadingAllowFragments | NSJSONReadingMutableContainers)
                     error: &error];
    
    if (!jsonArray){
        [self cancelTasks];
    }
    else {
        for(NSDictionary *item in jsonArray){
            [ [UserAlbumData sharedInstance] loadSingleUser:item];
        }
        [self.task2 resume];
    }
}



-(void) sendREST_GetUsers:(NSString *) urlString
{

    if([urlString length] <= 0)
    {
        // problems
        [self cancelTasks];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *requestUrl = [NSMutableURLRequest requestWithURL:url
                                                              cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                          timeoutInterval:4.0];
    
    [requestUrl setValue:@"Mozilla/5.0" forHTTPHeaderField:@"User-Agent"];
    [requestUrl addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    requestUrl.HTTPMethod = @"GET";
    
    session = [NSURLSession sharedSession];
    task1 = [session dataTaskWithRequest:requestUrl
            completionHandler: ^(NSData *rtndata, NSURLResponse *response, NSError *error)
    {
        if (error != nil && error.code == NSURLErrorTimedOut)
        {
            [self cancelTasks];
        }
        else if (error != nil){
            [self cancelTasks];            
        }
                
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;

        /*
        200 OK
        Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request, the response will contain an entity describing or containing the result of the action.
        */
        if([httpResponse statusCode] == 200)
        {
            if( ([rtndata length] > 0) && (error == nil) )
            {
                // now pass the data to a main threaded procedure, required by UIKit Responder chain,
                // parse the json data, persist message data
                [self performSelectorOnMainThread:@selector(recvUsersData:)
                                    withObject:rtndata waitUntilDone:NO];
                                                                
            }
            else if ([rtndata length] == 0 && error == nil) {
                //[self performSelectorOnMainThread:@selector(emptyReply:) withObject:nil waitUntilDone:NO];
                [self cancelTasks];
            }
        }
    }];
    
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"Main_UsersPhotos_Segue"])
    {
        [segue.destinationViewController setTitle:@"Choose A Users"];
        segue.destinationViewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    }
    else if ([[segue identifier] isEqualToString:@"Network_Trouble_Segue"]) {
        
    }

}


@end
