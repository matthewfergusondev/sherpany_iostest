//
//  DetailPhotoAlbumVC.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/23/16.
//

#import "DetailPhotoAlbumVC.h"

@interface DetailPhotoAlbumVC ()

@end

@implementation DetailPhotoAlbumVC

@synthesize  photoTitleDetail;
@synthesize  photoWebViewDetail;
@synthesize  photoDetails;



- (void)viewDidLoad {
    
    //setting the nav bar font.
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:28.0f];
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    [self setTitle:@"  "];
    [super viewDidLoad];
    
}



- (void) viewWillAppear:(BOOL)animated {
    [self performSelectorOnMainThread:@selector(loadWebView) withObject:nil waitUntilDone:NO];
    [super viewWillAppear:YES]; // forces a reload of the heirarchy
}


-(void) loadWebView
{

    self.photoTitleDetail.text = [NSString stringWithFormat:@"%@",photoDetails.photoTitle];
    
    self.photoWebViewDetail.scalesPageToFit = YES;
    self.photoWebViewDetail.autoresizesSubviews = YES;
    self.photoWebViewDetail.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.photoWebViewDetail.userInteractionEnabled = NO;
    self.photoWebViewDetail.backgroundColor = [UIColor clearColor];
    self.photoWebViewDetail.opaque = NO;
    [self.photoWebViewDetail setOpaque:NO];
    self.photoWebViewDetail.delegate = (id)self;
    NSURL * url = [NSURL URLWithString:photoDetails.photoUrl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReturnCacheDataElseLoad  timeoutInterval:5.0f];
    [self.photoWebViewDetail loadRequest:requestObj];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
}



- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error  {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
