//
//  AlbumPhotosVC.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "AlbumPhotosVC.h"

@interface AlbumPhotosVC ()

@end

@implementation AlbumPhotosVC

@synthesize stagingTargetAlbumId;
@synthesize myDataSourceArray;
@synthesize stagingAlbumPI;



- (void)viewDidLoad {

    [super viewDidLoad];

    //setting the nav bar font.
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:28.0f];
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    [self setTitle:@"Choose an Album"];

    myDataSourceArray = [[NSMutableArray alloc]init];
}



- (void) viewWillAppear:(BOOL)animated
{
    myDataSourceArray = [[UserAlbumData sharedInstance] retrieveAlbumPhotosInfo:(NSInteger)stagingTargetAlbumId];
    [super viewWillAppear:YES]; // forces a reload of the heirarchy
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [myDataSourceArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PhotoTitleTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"AlbumPhotoCustomCell_ID" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[PhotoTitleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AlbumPhotoCustomCell_ID"];
    }

    AlbumPhotoInfo * albumPI = [myDataSourceArray objectAtIndex:indexPath.row];
    [cell setCellPhotos:albumPI];
    
    return cell;

}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    stagingAlbumPI = [myDataSourceArray objectAtIndex:indexPath.row];
    [self setTitle:self.stagingAlbumPI.photoTitle];
    [self performSegueWithIdentifier:@"AlbumPhoto_DetailPhoto_Segue" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    DetailPhotoAlbumVC* userViewController = [segue destinationViewController];
    
    userViewController.photoDetails = [[AlbumPhotoInfo alloc]init];
    userViewController.photoDetails.photoTitle = stagingAlbumPI.photoTitle;
    userViewController.photoDetails.photoUrl = stagingAlbumPI.photoUrl;

}


@end
