//
//  UsersVC.h
//  SHERPANYiOSTest
//
//

#import <UIKit/UIKit.h>
#import "UserTableViewCell.h"
#import "UserAlbumData.h"
#import "AlbumNameVC.h"

@interface UsersVC : UIViewController

@property (nonatomic, strong) IBOutlet  UITableView   * myTableView;
@property (nonatomic, strong) IBOutlet  UISearchBar   * sBar;//search bar

@property (nonatomic, strong) NSIndexPath * singleUserIndex;


@end
