//
//  AlbumNameVC.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//


#import <UIKit/UIKit.h>
#import "AlbumNameTableViewCell.h"
#import "UserAlbumData.h"
#import "AlbumPhotosVC.h"


@interface AlbumNameVC : UIViewController {
    
                //index to a user that owns the album
                //related (relational)
                NSIndexPath * mySingleUserIndex;
                NSNumber * stagingAlbumId;
    
    IBOutlet    UITableView   * myTableView;
    
                NSMutableArray * myDataSourceArray;
    
                SingleUser * tempSU;
}


@property (nonatomic, strong) NSIndexPath * mySingleUserIndex;
@property (nonatomic, strong) NSNumber * stagingAlbumId;

@property (nonatomic, strong) IBOutlet  UITableView   * myTableView;

@property (nonatomic, strong) NSMutableArray   * myDataSourceArray;

@property (nonatomic, strong) SingleUser * tempSU;

@end
