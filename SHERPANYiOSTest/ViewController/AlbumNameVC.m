//
//  AlbumNameVC.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "AlbumNameVC.h"

@interface AlbumNameVC ()

@end



@implementation AlbumNameVC


@synthesize myTableView;
@synthesize mySingleUserIndex;
@synthesize tempSU;
@synthesize myDataSourceArray;
@synthesize stagingAlbumId;




- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //setting the nav bar font.
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:28.0f];
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    tempSU = [[[UserAlbumData sharedInstance] usersArray] objectAtIndex:mySingleUserIndex.row];

}



- (void) viewWillAppear:(BOOL)animated {
    
    myDataSourceArray = [tempSU.userList copy];
    [self setTitle:tempSU.name];
    [super viewWillAppear:YES];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [myDataSourceArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlbumNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AlbumNameCustomCell_ID" forIndexPath:indexPath];
    
    if (cell == nil){
        cell = [[AlbumNameTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AlbumNameCustomCell_ID"];
    }
    
    [cell setCellData:[myDataSourceArray objectAtIndex:indexPath.row]];
    
    return cell;
}



- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary * tempUserAlbumBinding = [myDataSourceArray objectAtIndex:indexPath.row];
    self.stagingAlbumId = [tempUserAlbumBinding objectForKey:@"id"];
    [self performSegueWithIdentifier:@"Users_AttachedAlbums_Segue" sender:self];
    
}


#pragma mark - Navigation


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AlbumPhotosVC* userViewController = (AlbumPhotosVC*)[segue destinationViewController];
    userViewController.stagingTargetAlbumId = [self.stagingAlbumId integerValue];//NSInteger convert from NSNumber
}


@end
