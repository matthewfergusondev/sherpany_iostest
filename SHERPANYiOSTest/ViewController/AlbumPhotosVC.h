//
//  AlbumPhotosVC.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import <UIKit/UIKit.h>
#import "PhotoTitleTableViewCell.h"
#import "UserAlbumData.h"
#import "DetailPhotoAlbumVC.h"

@interface AlbumPhotosVC : UIViewController


@property (nonatomic, strong) IBOutlet  UITableView   * myTableView;
@property (nonatomic, strong) NSMutableArray   * myDataSourceArray;

@property (nonatomic, assign) NSInteger stagingTargetAlbumId;

@property (nonatomic, assign) AlbumPhotoInfo * stagingAlbumPI;


@end
