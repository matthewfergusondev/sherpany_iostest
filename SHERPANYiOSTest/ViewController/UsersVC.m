//
//  UsersVC.m
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/22/16.
//

#import "UsersVC.h"

@interface UsersVC ()

@end


@implementation UsersVC


@synthesize myTableView, sBar;
@synthesize singleUserIndex;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    //setting the nav bar font.
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Light" size:28.0f];
    NSMutableDictionary *navBarTextAttributes = [NSMutableDictionary dictionaryWithCapacity:1];
    [navBarTextAttributes setObject:font forKey:NSFontAttributeName];
    self.navigationController.navigationBar.titleTextAttributes = navBarTextAttributes;
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    singleUserIndex = [[NSIndexPath alloc]init];
}


- (void) viewWillAppear:(BOOL)animated {
    
    [self setTitle:@"Choose A User"];
    [super viewWillAppear:YES];
}


- (void) viewDidAppear:(BOOL)animated {

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[UserAlbumData sharedInstance] usersArray] count];
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UserTableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:@"UserCustomCell_ID" forIndexPath:indexPath];
    
    if (cell == nil){
        cell = [[UserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:@"UserCustomCell_ID"];
    }
    
    [cell setCellUser:[[[UserAlbumData sharedInstance] usersArray] objectAtIndex:indexPath.row]];
    
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    singleUserIndex = indexPath;
    [self performSegueWithIdentifier:@"Users_AttachedAlbums_Segue" sender:self];
}


#pragma mark UISearchBarDelegate

// Delegate called when user enters/deletes text. Or the Cancel X is hit.
- (void) searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText {
    
    if ([searchText length] == 0) {
        sBar.showsCancelButton = NO;
        [[UserAlbumData sharedInstance] swapToOriginalModel];
        [self.myTableView reloadData];
    }
    
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
    [searchBar becomeFirstResponder];
    sBar.showsCancelButton = YES;
    [[UserAlbumData sharedInstance] swapToSearchModel];
    [self.myTableView reloadData];
    self.navigationController.navigationBar.topItem.title = @"Search for User";
    
}




- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
    sBar.showsCancelButton = NO;
    [[UserAlbumData sharedInstance] swapToOriginalModel];
    [self.myTableView reloadData];
    
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    
    @try{
        [[UserAlbumData sharedInstance] swapToOriginalModel];
        [self.myTableView reloadData];
    }
    @catch(NSException *e){
        NSLog(@"Threw an error  %@", e);
    }

    [searchBar resignFirstResponder];
    sBar.text = @"";
    self.navigationController.navigationBar.topItem.title =  @"Choose a User";
    
}



// called when Search button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [searchBar resignFirstResponder];
    
    NSArray * localUserArray = [[[UserAlbumData sharedInstance] usersArray]copy];
    
    SingleUser * tempSingleUser;
    BOOL found = NO;
    
    for( tempSingleUser in localUserArray ) {
        
        found = NO;
        
        if([tempSingleUser.name rangeOfString: searchBar.text options: NSCaseInsensitiveSearch].location != NSNotFound)
        { found = YES;}
        
        // designed to take add on search fields.
        if(found)
        {
            [[UserAlbumData sharedInstance] loadSearchUser:tempSingleUser];
        }
        
    }
    
    [[UserAlbumData sharedInstance] swapToSearchModel];
    [self.myTableView reloadData];
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    AlbumNameVC* userViewController = [segue destinationViewController];
    userViewController.mySingleUserIndex = singleUserIndex;    
}



@end
