//
//  DetailPhotoAlbumVC.h
//  SHERPANYiOSTest
//
//  Created by Matthew Ferguson on 2/23/16.
//

#import <UIKit/UIKit.h>
#import "AlbumPhotoInfo.h"

@interface DetailPhotoAlbumVC : UIViewController <UIWebViewDelegate>{
    IBOutlet UILabel * photoTitleDetail;
    IBOutlet UIWebView * photoWebViewDetail;
    
    AlbumPhotoInfo* photoDetails;
}


@property (nonatomic, strong) IBOutlet UILabel * photoTitleDetail;
@property (nonatomic, strong) IBOutlet UIWebView * photoWebViewDetail;

@property (nonatomic, strong) AlbumPhotoInfo * photoDetails;

-(void) loadWebView;

@end
